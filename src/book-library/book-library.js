import { OrbitControls } from './3d_path/OrbitControls.js';
import { GLTFLoader } from './3d_path/GLTFLoader.js';
import { OBJLoader } from './3d_path/OBJLoader.js';

class Item {
    id;
    name;
    type = ItemType;
    coordinates = { x: number, y: number, z: number };
    color;
    parentCoordinates = { x: number, y: number, z: number };
}

const ItemType = {
    WAREHOUSE: "Warehouse",
    SHELF: "Shelf",
    BOOK: "Book"
  }

var camera, mapCamera, scene, renderer, controls, light, bookGeometry, grid, board, startTile;
var gltfLoader = new GLTFLoader();
var objLoader = new OBJLoader();
var shelves = [], bookDataList = [];

const MINI_MAP = {
    WIDTH: 160,
    HEIGHT: 240
};

const GRID_CONFIG = {
    LENGTH: 45,
    BREADTH: 82,
    SIZE: 70,
    CELL_SIZE: 10
};

const COLOR = {
    SCENE: '#766C58',
    PATH: '0xffffff',
    UNWALKABLE: '0xff0000',
    PRE_SELECT: '0x00CCCC'
};

init();
animate();

function init() {
    // RENDERER
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.outputEncoding = THREE.sRGBEncoding;
    document.body.appendChild(renderer.domElement);

    // SCENE
    scene = new THREE.Scene();
    scene.background = new THREE.Color(COLOR.SCENE);

    // CAMERA SETTINGS
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 20000);
    camera.position.y = 40;
    camera.position.z = 110;

    // LIGHT SETTINGS
    light = new THREE.AmbientLight(0xffffff);
    light.position.set = (0,100,100);
    scene.add(light);
  
    var dlight = new THREE.DirectionalLight( 0xaabbff, 1 );
    dlight.position.x = 0;
    dlight.position.y = 750;
    dlight.position.z = 0;
    dlight.castShadow = true;
    dlight.shadow.camera.top = 2500;
    dlight.shadow.camera.bottom = - 2500;
    dlight.shadow.camera.left = - 2500;
    dlight.shadow.camera.right = 2500;
    dlight.shadow.camera.near = 1;
    dlight.shadow.camera.far = 1000;
    dlight.shadow.mapSize.set( 2048, 2048 );
    scene.add( dlight );

    // ORBIT CONTROLS
    controls = new OrbitControls(camera, renderer.domElement);
    controls.target = new THREE.Vector3(0, 0, 0);
    controls.enableDamping = true;
    controls.dampingFactor = 0.25;

    bookGeometry = getBookGeometry();
    // loadWarehouse();
    loadRacks();
    initSearchlib()
}

function initSearchlib(){
    document.getElementById('popup-search-book').addEventListener('click', openSearchModel)
    document.getElementById('mysearchInput').addEventListener('keyup', filterFunction)

    var dropdown = document.getElementById('books-drop-down')
    getBooks().then(function (data) {
        data.forEach(function(entry, index){
            var atag = document.createElement("a")
            atag.text = entry.name
            atag.addEventListener('click', function(){
              updateSelection(entry)
            })
            dropdown.appendChild(atag)
        });
    });

    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the button that opens the modal
    var btn = document.getElementById("search_book");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on the button, open the modal
    btn.onclick = function () {
        modal.style.display = "block";
        document.getElementById("mysearchInput").value = ''
        filterFunction()
        closeSearchModel()
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    // window.onclick = function (event) {
    //     if (event.target == modal) {
    //         modal.style.display = "none";
    //     }
    // }
}
  function openSearchModel() {
    document.getElementById("books-drop-down").classList.toggle("show");
    if(!document.getElementById("books-drop-down").classList.contains('show')){
      document.getElementById("books-drop-down").classList.toggle("show");
    }
    var input = document.getElementById("mysearchInput");
    if(input.value.length == 0 && document.getElementById('books-drop-down').scrollTop > 0){
      document.getElementById('books-drop-down').scrollTop = 0
    }
  }

  function closeSearchModel() {
    if(document.getElementById("books-drop-down").classList.contains('show')){
      document.getElementById("books-drop-down").classList.toggle("show");
    }
  }

  function filterFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("mysearchInput");
    filter = input.value.toUpperCase();
    let div = document.getElementById('books-drop-down');
    a = div.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
      let txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = "";
      } else {
        a[i].style.display = "none";
      }
    }
  }

  function updateSelection(book){
    if(book != null && book != undefined){
        generatePath(book)
        resetSearchModel()
    }
  }
  function resetSearchModel(){
    var modal = document.getElementById("myModal");
    document.getElementById("books-drop-down").classList.toggle("show");
    modal.style.display = "none";
    var input = document.getElementById("mysearchInput")
    input.value = ""
    filterFunction()
  }

function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
    controls.update();
}

function constructGrid() {
    grid = new SqrGrid({
      cellSize: GRID_CONFIG.CELL_SIZE
    });

    grid.generate({
      length: GRID_CONFIG.LENGTH,
      breadth: GRID_CONFIG.BREADTH,
      size: GRID_CONFIG.SIZE
    });

    board = new Board(grid, {
      allowDiagonal: true,
      // this is the fallback, but you can override it by passing a specific heuristic in board.findPath()
      heuristicFilter: isCellTraversable
    });

    board.generateTilemap();
    scene.add(board.group);
    setUnwalkableTiles();
    displayStartTile();
}

function setUnwalkableTiles() {
    var obstacles = getObstaclePositions();
    obstacles.forEach((obstacle) => {
      var cell = board.grid.getCellAt(new THREE.Vector3(obstacle.x, obstacle.y, obstacle.z));
      if (cell) {
        cell.walkable = false;
        // if (cell.tile) {
        //   highlightTile(cell.tile, COLOR.UNWALKABLE);
        // }
      }
    });
  }

  function displayStartTile() {
    var startTileIndex = Math.ceil((GRID_CONFIG.BREADTH * (GRID_CONFIG.LENGTH + 2)) / 2);
    startTile = board.tiles[startTileIndex];
    highlightTile(startTile, COLOR.PATH);
  }

  function highlightTile(tile, color) {
    if (tile.material.emissive) {
      tile.material.emissive.setHex(color);
    }
    tile.highlight = color;
  }

  function getObstaclePositions() {
    var obstacles = [];

    shelves.forEach(shelf => {
      var box = new THREE.Box3().setFromObject(shelf);
      obstacles.push(box.min);
      obstacles.push(box.max);
      obstacles.push(shelf.position);
    });

    return obstacles;
  }

  function createPath(startTile, endTile) {
    grid.traverse(deselectVisited);
    var path = board.findPath(startTile, endTile);
    // highlight the path
    if (path) {
      for (var i = 0; i < path.length; i++) {
        if (path[i].tile) {
          highlightTile(path[i].tile, COLOR.PATH);
        }
      }
      // see which tiles were visited
      grid.traverse(selectVisited);
    }
  }

  function selectVisited(c) {
    if (c._visited) {
      c.tile.select();
    }
  }

  function deselectVisited(c) {
    if (c.tile.selected) {
      c.tile.deselect();
    }
  }

  function isCellTraversable(origin, next) {
    // example of how to filter out neighbors that are too tall to traverse
    // but allows the algorithm to "jump" down to whatever depth
    if (next.h - origin.h > 10) {
      return false; // no, filter out next
    }
    return true; // yes, keep next for consideration
  }

  var arrowHelper = null;

  function generatePath(book) {
    if(arrowHelper){
      scene.remove(arrowHelper);
    }
    var endTile = getEndTile(book.parentCoordinates);
    if (!endTile) {
      endTile = startTile;
    }
    if (startTile.cell.equals(endTile.cell)) {
      // they're on top of each other, so place one at a diagonal of the other
      var neighbors = grid.getNeighbors(startTile, true);
      // the diagonals are added to the list after all the others are, so we pick the last one
      endTile = neighbors[neighbors.length - 1];
    }
    createPath(startTile, endTile);
    var to = new THREE.Vector3(book.coordinates.x, book.coordinates.y, book.coordinates.z);
    var origin = new THREE.Vector3(book.coordinates.x, book.coordinates.y - 5, book.coordinates.z + 20);
    var direction = to.clone().sub(origin);
    var length = direction.length();
    var hex = 0xffff00;

    arrowHelper = new THREE.ArrowHelper(direction.normalize(), origin, length, hex);
    scene.add(arrowHelper);
  }

  function getEndTile(shelfCoordinate) {
    var shelfPosition = new THREE.Vector3(shelfCoordinate.x, shelfCoordinate.y, shelfCoordinate.z);
    var cell = board.grid.getCellAt(shelfPosition);
    if (cell && cell.tile) {
      return cell.tile;
    }
    return null;
  }

function loadWarehouse() {
    gltfLoader.load('../../assets/3d_models/warehouse1/scene.gltf', gltf => {
        var box = new THREE.Box3().setFromObject(gltf.scene);
        gltf.scene.position.z = (box.max.z - box.min.z) / 2
        gltf.scene.position.x = -(box.max.x - box.min.x) / 2
        scene.add(gltf.scene);
    });
}

function loadRacks() {
    objLoader.load('../../assets/3d_models/shelf1/bookcase.obj', (object) => {
      object.traverse(function (child) {
        if (child.isMesh) {
          child.material = new THREE.MeshNormalMaterial();
        }
      });

      shelves = [];
      getItems([ItemType.SHELF, ItemType.BOOK]).then(data => {
        if (data && data.length) {
          var shelfDataList = [];
          shelfDataList = data.filter(x => x.type == ItemType.SHELF);
          shelfDataList.forEach(shelf => {
            var shelfObject = object.clone();
            shelfObject.name = shelf.type + '_' + shelf.id;
            shelfObject.scale.set(40, 30, 40);
            shelfObject.position.set(shelf.coordinates.x, shelf.coordinates.y, shelf.coordinates.z);
            shelves.push(shelfObject);
            scene.add(shelfObject)
          });
          bookDataList = data.filter(x => x.type == ItemType.BOOK);
          bookDataList.forEach(book => {
            var bookObj = getBookObject(bookGeometry);
            bookObj.name = book.type + '_' + book.id;
            bookObj.position.set(book.coordinates.x, book.coordinates.y, book.coordinates.z);
            scene.add(bookObj)
          });
        }
        constructGrid()
      });
    });
}

function getBookGeometry() {
    const width = 1.4;
    const height = 6;
    const depth = 5.9;
    return new THREE.BoxGeometry(width, height, depth);
  }

function getBookObject(bookGeometry) {
    const material = new THREE.MeshPhongMaterial({ color: Math.random() * 0xffffff });
    material.transparent = true;
    const bookObject = new THREE.Mesh(bookGeometry.clone(), material);
    return bookObject;
}

async function getItems() {
  let response = await fetch(constants.ggk_geek_JsonServerUrl + '/items?type=Shelf&type=Book')
  let data = await response.json()
  return data;
}

async function getBooks() {
  let response = await fetch(constants.ggk_geek_JsonServerUrl + '/items?type=Book')
  let data = await response.json()
  return data;
}
