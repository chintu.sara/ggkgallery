var constants = {
    ggk_geek_baseURL: "https://geekreboot2020.netlify.app/",
    ggk_geek_JsonServerUrl: "http://52.186.66.93",
    message: {
      gallery: 'Hey, visit our gallery to view SOMs, TalkMasters & Newsletters !',
      library: 'Hi... Are you interested in any book ? Search in our library !',
      game: 'Are you bored ? Check out our games and play as long as you wish !',
      intro: 'Welcome to Exhibition... !!! Explore Gallery, Library & Games',
      empty: ''
    },
    section: {
      gallery: 'GALLERY',
      library: 'LIBRARY',
      game: 'GAME',
      intro: 'INTRO',
      SOM_podium:'podium SM',
      talkmasterStage_podium: 'podium TM',
      newsletterStage_podium: 'podium NL',
      mazeStage_podium: 'podium MZ',
      gameStage_podium: 'podium GS',
      libraryStage_podium: 'podium LB',
    },
    url:{
      SOM_podium:'/src/gallery/som/som.html',
      talkmasterStage_podium: '/src/gallery/talkmaster/talkmaster.html',
      newsletterStage_podium: '/src/gallery/newsletter/newsletter.html',
      mazeStage_podium: '/src/games/maze-game',
      gameStage_podium: '/src/games/car-reboot',
      libraryStage_podium: '/src/book-library/book-library.html',
    }
};

const frameSettings = {
  som: {
    position: {
      x: -900, 
      y: 250,
      z: -700
    },
    image: '/assets/img/som.jpg',
    color: 0xA0590D
  },
  talkmaster: {
    position: {
      x: -800,
      y: 250,
      z: -2100
    },
    image: '/assets/img/talkmaster.jpg',
    color: 0xA0590D
  },
  newsletter: {
    position: {
      x: 0,
      y: 250,
      z: -2100
    },
    image: '/assets/img/newsletter.jpg',
    color: 0xA0590D
  },
  mazeGame: {
    position: {
      x: 2200,
      y: 250,
      z: -800
    },
    image: '/assets/img/maze.png',
    color: 0xA0590D
  },
  carGame: {
    position: {
      x: 2200,
      y: 250,
      z: 0
    },
    image: '/assets/img/car.jpg',
    color: 0xA0590D
  },
  library: {
    position: {
      x: -2200,
      y: 250,
      z: -500
    },
    image: '/assets/img/library.png',
    color: 0xA0590D
  }
}

const extrudeSettings = { 
  depth: 8,
  bevelEnabled: true,
  bevelSegments: 2,
  steps: 2,
  evelSize: 1,
  bevelThickness: 1
};

const WalkKey = {
  LEFT: 'a',
  RIGHT: 'd',
  UP: 'w',
  DOWN: 's'
};

function navigateToHome(){
  window.location.href = constants.ggk_geek_baseURL + "/src"
}

