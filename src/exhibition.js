import { OrbitControls } from '../assets/js/OrbitControls.js';
import { GLTFLoader } from '../assets/js/GLTFLoader.js';
import { TTFLoader } from '../assets/js/TTFLoader.js';

var camera, scene, renderer, controls, messageSprite, object3DPlane, frame, frameMaterial, podiumGroup;
var colorTimer = null;
var libraryTextSprite, gameTextSprite, galleryTextSprite, introTextSprite; 
var mixer = new THREE.AnimationMixer(), mixerW = new THREE.AnimationMixer(), mixerG = new THREE.AnimationMixer(), mixerb = new THREE.AnimationMixer(),
mixerBoy = new THREE.AnimationMixer();
var man_walk = new THREE.Scene();
var keyboard = new THREEx.KeyboardState();
var clock = new THREE.Clock();
var gltfLoader = new GLTFLoader();
var raycaster = new THREE.Raycaster();
var interactiveObjects = [];
var group, textMesh1, textMesh2, textGeo, material;
var firstLetter = true;
var seed = 6;
var text = 'three.js',
				height = 10,
				size = 20,
				hover = 30,
				curveSegments = 4,
				bevelThickness = 1,
        bevelSize = 1.5;
var font = null;
var listener = new THREE.AudioListener();
var loader = new THREE.FontLoader();
var sound = new THREE.Audio( listener );
var sound_walk = new THREE.Audio( listener );
var audioLoader = new THREE.AudioLoader();

init();
animate();

function init(){
  //Font loader

  THREE.Cache.enabled = true;
  // RENDERER
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.outputEncoding = THREE.sRGBEncoding;
  renderer.shadowMap.enabled = true;
  document.body.appendChild(renderer.domElement);

  // SCENE
  scene = new THREE.Scene();  
  // scene.background = new THREE.Color( 0xC3D8D6 );
  scene.fog = new THREE.FogExp2( 0xd6f7ff, 0.0001 );

  // CAMERA
  camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 19000 );
  camera.position.x = 70;
  camera.position.y = 200;
  camera.position.z = 2340;
  camera.add( listener );

  //skybox
  var imagePrefix = "games/car-reboot/textures/skybox/";
	var directions  = ["xpos", "xneg", "ypos", "yneg", "zpos", "zneg"].reverse();
  var imageSuffix = ".jpg";
  
  var skyGeometry = new THREE.BoxGeometry( 25000, 25000, 10000 );
  var materialArray = [];
  for (var i = 0; i < 6; i++){
    	materialArray.push( new THREE.MeshPhongMaterial({
			map: THREE.ImageUtils.loadTexture( imagePrefix + directions[i] + imageSuffix ),
			side: THREE.DoubleSide
		}));
  }
	var skyMaterial = new THREE.MeshFaceMaterial( materialArray );
  var skyBox = new THREE.Mesh( skyGeometry, skyMaterial );
  skyBox.position.y = 2000;
  scene.add( skyBox );
  //end skybox

  // LIGHTS
  setLights();

  // TEXT SPRITES
  generateTextMessages();

  // ORBIT CONTROLS
  setOrbitControls();

  // PLANE FOR 3D MODELS
  object3DPlane = createObject3DPlane();

  // FLOOR
  designFloor();

  // 3D MODELS
  loadBalloonBear();
  loadTentAndPodiums();
  loadWoman();
  loadSchoolGirl();
  loadGameBoy()
  loadMan();
  loadIntroModel();
  loadFrames();

  createText('PLAY GAMES',0xff0000, 1200, 0, 0, 0, 200, 0, -26, 0, 10);
  createText('GALLERY',0x74AE23, 200, 0, -1200, 0, 0, 0, 2, 0, 25);
  createText('LIBRARY',0x2b00ff, -1500, 0, 700, 0, -200.4, 0, 20, 0, 10);
  audioLoader.load( "../assets/sounds/bgm.wav", function( buffer ) {
    sound.setBuffer( buffer );
    sound.setLoop( true );
    sound.setVolume( 0.05 );
    sound.play();
  });
  audioLoader.load( "../assets/sounds/walk.mp3", function( buffer ) {
    sound_walk.setBuffer( buffer );
    sound_walk.setLoop( true );
    sound_walk.setVolume( 0.2 );
  });
}
function createText(text, colors, xpos, ypos, zpos, xrot, yrot, zrot, lxpos, lypos, lzpos){
  var tmesh;
  loader.load( '../assets/font/helvetiker_bold.typeface.json', function ( font ) {

    var textGeo = new THREE.TextBufferGeometry( text, {
      font: font,
      size: 100,
      height: 50,
      curveSegments: 12,
      bevelThickness: 2,
      bevelSize: 1,
      bevelEnabled: true

    } );

    textGeo.computeBoundingBox();
    var centerOffset = - 0.5 * ( textGeo.boundingBox.max.x - textGeo.boundingBox.min.x );

    var textMaterial = new THREE.MeshPhongMaterial( { color: colors } );

    tmesh = new THREE.Mesh( textGeo, textMaterial );
    tmesh.position.x = xpos;
    tmesh.position.y = ypos;
    tmesh.rotation.y = yrot;
    tmesh.position.z = zpos;
    tmesh.castShadow = true;
    tmesh.receiveShadow = true;
    // light.target = tmesh;
    scene.add( tmesh );

  } );

}

function setLights() {
  const light = new THREE.AmbientLight(0xffffff);

  var hlight = new THREE.HemisphereLight( 0xffffbb, 0xcdd1ce, 1.3 );
  scene.add(hlight);

  var dlight = new THREE.DirectionalLight( 0xaabbff, 1 );
  var dlight2 = new THREE.DirectionalLight( 0xaabbcc, 1 );
  dlight2.position.x = -500;
  // dlight2.position.y = - 150;
  dlight.position.x = 500;
  dlight.position.y = 750;
  dlight.position.z = 100;
  dlight.castShadow = true;
  dlight.shadow.camera.top = 2500;
  dlight.shadow.camera.bottom = - 2500;
  dlight.shadow.camera.left = - 2500;
  dlight.shadow.camera.right = 2500;
  dlight.shadow.camera.near = 1;
  dlight.shadow.camera.far = 15000;
  dlight.shadow.mapSize.set( 2048, 2048 );
  scene.add( dlight, dlight2 );
}

function generateTextMessages() {
  libraryTextSprite = makeTextSprite(constants.message.library);
  galleryTextSprite = makeTextSprite(constants.message.gallery);
  gameTextSprite = makeTextSprite(constants.message.game);
  introTextSprite = makeTextSprite(constants.message.intro);
}

function setOrbitControls() {
  controls = new OrbitControls(camera, renderer.domElement);
  // controls.enableDampling = true;
  // controls.campingFactor = 0.25;
  // controls.enableZoom = true;
  controls.maxPolarAngle = Math.PI * 0.5;
  controls.minDistance = 1000;
  controls.maxDistance = 5000;
}

// Animate
function animate() {
  requestAnimationFrame(animate); 
  var delta = clock.getDelta(); // seconds.
  mixerW.update(delta);
  mixerb.update(delta*2);
  mixerBoy.update(delta * 4);
  mixerG.update(delta);
  render();
  updatePodiumColors();
  updateMan();
}

function render() {
	renderer.render( scene, camera );
}

/********************************** START : LOAD 3D MODELS *************************************/
function loadMan(){
  gltfLoader.load('/assets/3d_models/boy_animated/scene.gltf', gltf => {
    man_walk = gltf.scene;
    man_walk.position.set(0, 3, 2050);
    man_walk.rotateY(210.5);
    gltf.scene.traverse( function ( object ) {
      if ( object.isMesh ) object.castShadow = true;
    });
    scene.add(man_walk);
    man_walk.scale.multiplyScalar(1.2);
    mixer = new THREE.AnimationMixer(man_walk);
    mixer.clipAction(gltf.animations[0]).play();
  });
}

function loadWoman(){
  gltfLoader.load('/assets/3d_models/woman/scene.gltf', gltf => {
    gltf.scene.traverse( function ( object ) {
      if ( object.isMesh ) object.castShadow = true;
    });
    var woman = gltf.scene;
    woman.position.set(-300, 0, -500);
    scene.add(woman);
    woman.scale.multiplyScalar(1.3);

    mixerW = new THREE.AnimationMixer(woman);
    mixerW.clipAction(gltf.animations[0]).play();

    var plane = object3DPlane.clone();
    scene.add( plane );
    plane.userData = setUserDataOf3DModelsForText(woman, constants.message.gallery, constants.section.gallery);
    plane.position.set(woman.position.x, woman.position.y, woman.position.z)
    interactiveObjects.push(plane);
  });
}

function loadBalloonBear() {
  gltfLoader.load('/assets/3d_models/bear_on_balloons/scene.gltf', gltf => {
    gltf.scene.traverse( function ( object ) {
      if ( object.isMesh ) object.castShadow = true;
    });

    var bear = gltf.scene;
    bear.scale.set(50,50,50);
    bear.position.set(300,150,900);
    scene.add(bear);

    mixerb = new THREE.AnimationMixer(bear);
    mixerb.clipAction(gltf.animations[0]).play();

  });
}

function loadSchoolGirl() {
  gltfLoader.load('/assets/3d_models/girl_idle/scene.gltf', gltf => {
    gltf.scene.traverse( function ( object ) {
      if ( object.isMesh ) object.castShadow = true;
    });
    var girl = gltf.scene;
    girl.position.set(-1200, 90, -100);
    girl.rotation.y = Math.PI/2
    scene.add(girl);
    girl.scale.multiplyScalar(70);

    mixerG = new THREE.AnimationMixer(gltf.scene);
    mixerG.clipAction(gltf.animations[0]).play();

    var plane = object3DPlane.clone();
    scene.add( plane );
    plane.userData = setUserDataOf3DModelsForText(girl, constants.message.library, constants.section.library);
    plane.position.set(girl.position.x, 20, girl.position.z)
    interactiveObjects.push(plane);
  });
}

function loadGameBoy() {
  gltfLoader.load('/assets/3d_models/gameboy/scene.gltf', gltf => {
    var gameBoy = gltf.scene;
    gameBoy.position.set(1000, 0, -80)
    gameBoy.rotateY(-20)
    scene.add(gameBoy);
    gameBoy.scale.multiplyScalar(52);
    gameBoy.traverse( function ( object ) {
      if ( object.isMesh ) object.castShadow = true;
    });

    mixerBoy = new THREE.AnimationMixer(gltf.scene);
    mixerBoy.clipAction(gltf.animations[0]).play();

    var plane = object3DPlane.clone();
    scene.add( plane );
    plane.userData = setUserDataOf3DModelsForText(gameBoy, constants.message.game, constants.section.game);
    plane.position.set(gameBoy.position.x, 20, gameBoy.position.z)
    interactiveObjects.push(plane);
  });
}

function loadIntroModel() {
  gltfLoader.load('/assets/3d_models/cute_boy/scene.gltf', gltf => {
    gltf.scene.traverse( function ( object ) {
      if ( object.isMesh ) object.castShadow = true;
    });
    var intro = gltf.scene;
    intro.position.set(-150, 20, 1500);
    scene.add(intro);
    intro.scale.multiplyScalar(60);
    intro.rotateY(-50);

    var plane = object3DPlane.clone();
    scene.add( plane );
    plane.userData = setUserDataOf3DModelsForText(intro, constants.message.intro, constants.section.intro);
    plane.position.set(intro.position.x, intro.position.y, intro.position.z)
    interactiveObjects.push(plane);
  });
}

/********************************** END : LOAD 3D MODELS *************************************/

/********************************** START : PODIUMS *************************************/
function getPodiumURL(type){
  switch(type){
    case constants.section.SOM_podium: return constants.url.SOM_podium;
    case constants.section.talkmasterStage_podium: return constants.url.talkmasterStage_podium;
    case constants.section.newsletterStage_podium: return constants.url.newsletterStage_podium;
    case constants.section.mazeStage_podium: return constants.url.mazeStage_podium;
    case constants.section.gameStage_podium: return constants.url.gameStage_podium;
    case constants.section.libraryStage_podium: return constants.url.libraryStage_podium;
  }
}

function loadTentAndPodiums(){
  gltfLoader.load('/assets/3d_models/single_edition_tent/scene.gltf', gltf => {
    var tent = gltf.scene;
    tent.rotateY(10);
    tent.scale.set(130,120,100);
    tent.position.set(-300, 0, -900);
    gltf.scene.traverse( function ( object ) {
      if ( object.isMesh ) object.castShadow = true;
    });
    scene.add(tent);
  });

  podiumGroup = new THREE.Group()

  var somStage = generateHexagonStage(extrudeSettings, -300, -810);
  podiumGroup.add(somStage);
  addPodiumAsIntractiveStage(somStage,constants.message.empty,constants.section.SOM_podium)
  
  var talkmasterStage = generateHexagonStage(extrudeSettings, -350, -1800);
  podiumGroup.add(talkmasterStage);
  addPodiumAsIntractiveStage(talkmasterStage,constants.message.empty,constants.section.talkmasterStage_podium)
  
  var newsletterStage = generateHexagonStage(extrudeSettings, 400, -1800);
  podiumGroup.add(newsletterStage);
  addPodiumAsIntractiveStage(newsletterStage,constants.message.empty,constants.section.newsletterStage_podium)

  var mazeStage = generateHexagonStage(extrudeSettings, 2000, -300);
  podiumGroup.add(mazeStage);
  addPodiumAsIntractiveStage(mazeStage,constants.message.empty,constants.section.mazeStage_podium)

  var gameStage = generateHexagonStage(extrudeSettings, 2000, 550);
  podiumGroup.add(gameStage);
  addPodiumAsIntractiveStage(gameStage,constants.message.empty,constants.section.gameStage_podium)

  var libraryStage = generateHexagonStage(extrudeSettings, -2000, 0);
  podiumGroup.add(libraryStage);
  addPodiumAsIntractiveStage(libraryStage,constants.message.empty,constants.section.libraryStage_podium)

  scene.add(podiumGroup);
}

function addPodiumAsIntractiveStage(stage, message, section){
    var plane = object3DPlane.clone();
    scene.add(plane);
    plane.userData = setUserDataOf3DModelsForText(stage, message, section);
    plane.objectType = 'podium';
    plane.position.set(stage.children[0].position.x, stage.children[1].position.y, stage.children[0].position.z)
    interactiveObjects.push(plane);
}

function updatePodiumColors() {
  if (colorTimer == null) {
    colorTimer = setInterval(
      function() {
        podiumGroup.children.forEach(podium => {
          var color = Math.random() * 0xffffff
          podium.children.forEach((object, index) => {          
            if(index % 2 == 0) {
              object.material.color = new THREE.Color(color);
            }
          })
        });
    }, 1000);
  }
}

/********************************** START : PODIUMS *************************************/

/********************************** START : FRAMES *************************************/

function loadFrames() {
  gltfLoader.load('/assets/3d_models/old-frame/scene.gltf', gltf => {
    frame = gltf.scene;
    var somFrame = frame.clone();
    somFrame.position.set(frameSettings.som.position.x, frameSettings.som.position.y, frameSettings.som.position.z);
    somFrame.traverse( function ( object ) {
      if (object.isMesh) {
        frameMaterial = object.material;
        updateFrameMaterial(object, frameSettings.som);
      }
    });
    scene.add(somFrame);
    somFrame.rotation.y = Math.PI/4

    var talkMasterFrame = loadFrameByType(frameSettings.talkmaster);
    scene.add(talkMasterFrame);

    var newsletterFrame = loadFrameByType(frameSettings.newsletter);
    scene.add(newsletterFrame);

    var carFrame = loadFrameByType(frameSettings.carGame);
    scene.add(carFrame);
    carFrame.rotation.y = -Math.PI/2

    var mazeFrame = loadFrameByType(frameSettings.mazeGame);
    scene.add(mazeFrame);
    mazeFrame.rotation.y = -Math.PI/2
   
    var libraryFrame = loadFrameByType(frameSettings.library);
    scene.add(libraryFrame);
    libraryFrame.rotation.y = -Math.PI/2
  });
}

function loadFrameByType(settings) {
  var portrait = frame.clone();
  portrait.position.set(settings.position.x, settings.position.y, settings.position.z);
  portrait.traverse( function ( object ) {
    if (object.isMesh) {
      object.material = frameMaterial.clone();
      updateFrameMaterial(object, settings);
    }
  });
  return portrait;
}

function updateFrameMaterial(object, settings) {
  if(object.name == "Plane_1_Mat2_0") {
    var newTexture = new THREE.TextureLoader().load( settings.image );
    newTexture.encoding = THREE.sRGBEncoding;
    object.material.map = newTexture;
    object.material.needsUpdate = true;
  } else {
    object.material.color = new THREE.Color(settings.color)
  }
  object.castShadow = true;
}

/********************************** END : FRAMES *************************************/

/********************************** START : FLOOR *************************************/
function designFloor() {

  // FLOOR
  var groundTexture = new THREE.TextureLoader().load( '/assets/img/grasslight-big.jpg' );
  groundTexture.wrapS = groundTexture.wrapT = THREE.RepeatWrapping;
  groundTexture.repeat.set( 25, 25 );
  groundTexture.anisotropy = 16;
  groundTexture.encoding = THREE.sRGBEncoding;

  var groundMaterial = new THREE.MeshLambertMaterial( { map: groundTexture } );
  var floor = new THREE.Mesh( new THREE.PlaneBufferGeometry( 20000, 20000 ), groundMaterial );
  floor.rotation.x = - Math.PI / 2;
  floor.receiveShadow = true;
  scene.add( floor );

  // ROADS
  buildRoads();

  // STAGES
  buildStages();
}

function buildRoads() {
  // Vertical road
  var roadGeometry1 = new THREE.BoxBufferGeometry( 1, 0, 8 );
  var roadMaterial1 = new THREE.MeshPhongMaterial( {color: 0x221F1E} );
  var road1 = new THREE.Mesh( roadGeometry1, roadMaterial1 );
  road1.scale.set(500,8,500);
  road1.position.set(0,0,500);
  road1.receiveShadow = true;
  scene.add(road1);

  // Horizontal road
  var roadGeometry2 = new THREE.BoxBufferGeometry( 6, 0, 1 );
  var roadMaterial2 = new THREE.MeshPhongMaterial( {color: 0x221F1E} );
  var road2 = new THREE.Mesh( roadGeometry2, roadMaterial2 );
  road2.scale.set(500,8,500);
  road2.receiveShadow = true;
  scene.add(road2);
}

function buildStages() {
  var stageGeometry1 = new THREE.BoxBufferGeometry( 10, 0, 10 );
  var stageMaterial1 = new THREE.MeshPhongMaterial( {color: 0x8B0000} );
  var stage1 = new THREE.Mesh( stageGeometry1, stageMaterial1 );
  stage1.scale.set(150,8,100);
  stage1.position.set(0,0,-2000);
  stage1.receiveShadow = true;

  var stage2 = stage1.clone();
  stage2.material = stage1.material.clone();
  stage2.material.color = new THREE.Color(0x191970);
  stage2.scale.set(100,8,150);
  stage2.position.set(2000,0,0);

  var stage3 = stage2.clone();
  stage3.position.set(-2000,0,0);
  stage3.material = stage2.material.clone();
  stage3.material.color = new THREE.Color(0x006400);

  scene.add(stage1);
  scene.add(stage2);
  scene.add(stage3);
}
/********************************** END : FLOOR *************************************/

/********************************** START : USER ANIMATION *************************************/
var previousClickedDirection = WalkKey.UP;
var forwordDirection = 1;
var directionClickCount = [];
directionClickCount[WalkKey.LEFT] = 0;
directionClickCount[WalkKey.RIGHT] = 0;
directionClickCount[WalkKey.UP] = 1;
directionClickCount[WalkKey.DOWN] = 0;

function updateMan() {
  var delta = clock.getDelta(); // seconds.
  mixerW.update(delta);
  var moveDistance = 1000 * delta; // 100 pixels per second
  if (keyboard.pressed(WalkKey.LEFT)) {
    if (directionClickCount[WalkKey.LEFT] == 0) {
      man_walk.rotateOnAxis(new THREE.Vector3(0, 1, 0), getDegreeToRotate(WalkKey.LEFT));
      directionClickCount[WalkKey.LEFT]++;
      previousClickedDirection = WalkKey.LEFT;
    }
    // sound_walk.play();
    man_walk.translateZ(moveDistance);
    camera.position.x-=moveDistance;
    mixer.update(delta * 4);
    ResetAllDirectionClickCountExceptMyDirection(WalkKey.LEFT);
  }
  if (keyboard.pressed(WalkKey.RIGHT)) {
    if (directionClickCount[WalkKey.RIGHT] == 0) {
      man_walk.rotateOnAxis(new THREE.Vector3(0, 1, 0), getDegreeToRotate(WalkKey.RIGHT));
      directionClickCount[WalkKey.RIGHT]++;
      previousClickedDirection = WalkKey.RIGHT;
    }
    // sound_walk.play();
    man_walk.translateZ(moveDistance);
    camera.position.x+=moveDistance;
    mixer.update(delta * 4);
    ResetAllDirectionClickCountExceptMyDirection(WalkKey.RIGHT);
  }
  if (keyboard.pressed(WalkKey.UP)) {
    if (directionClickCount[WalkKey.UP] == 0) {
      man_walk.rotateOnAxis(new THREE.Vector3(0, 1, 0), getDegreeToRotate(WalkKey.UP));
      directionClickCount[WalkKey.UP]++;
      previousClickedDirection = WalkKey.UP;
      forwordDirection = 1;
    }
    // sound_walk.play();
    man_walk.translateZ(moveDistance);
    camera.position.z-=moveDistance;
    mixer.update(delta * 4);
    ResetAllDirectionClickCountExceptMyDirection(WalkKey.UP);
  }
  if (keyboard.pressed(WalkKey.DOWN)) {
    if (directionClickCount[WalkKey.DOWN] == 0) {
      man_walk.rotateOnAxis(new THREE.Vector3(0, 1, 0), getDegreeToRotate(WalkKey.DOWN));
      directionClickCount[WalkKey.DOWN]++;
      previousClickedDirection = WalkKey.DOWN;
      forwordDirection = -1;
    }
    // sound_walk.play();
    man_walk.translateZ(moveDistance);
    camera.position.z+=moveDistance;
    mixer.update(delta * 4);
    ResetAllDirectionClickCountExceptMyDirection(WalkKey.DOWN);
  }
  showHideTextSprite();
  
}

function ResetAllDirectionClickCountExceptMyDirection(direction) {
  switch (direction) {
    case WalkKey.LEFT:
      directionClickCount[WalkKey.RIGHT] = 0;
      directionClickCount[WalkKey.UP] = 0;
      directionClickCount[WalkKey.DOWN] = 0;
      break;
    case WalkKey.RIGHT:
      directionClickCount[WalkKey.LEFT] = 0;
      directionClickCount[WalkKey.UP] = 0;
      directionClickCount[WalkKey.DOWN] = 0;
      break;
    case WalkKey.UP:
      directionClickCount[WalkKey.RIGHT] = 0;
      directionClickCount[WalkKey.LEFT] = 0;
      directionClickCount[WalkKey.DOWN] = 0;
      break;
    case WalkKey.DOWN:
      directionClickCount[WalkKey.RIGHT] = 0;
      directionClickCount[WalkKey.UP] = 0;
      directionClickCount[WalkKey.LEFT] = 0;
      break;
  }
}

function getDegreeToRotate(direction){
  switch (previousClickedDirection) {
    case WalkKey.LEFT:
      switch (direction){
        case WalkKey.RIGHT: return -Math.PI;
        case WalkKey.UP: return -Math.PI / 2; 
        case WalkKey.DOWN: return Math.PI / 2; 
      }
      break;
    case WalkKey.RIGHT:
      switch (direction){
        case WalkKey.LEFT: return Math.PI;
        case WalkKey.UP: return Math.PI / 2;
        case WalkKey.DOWN: return -Math.PI / 2;
      }
      break;
    case WalkKey.UP:
      switch (direction){
        case WalkKey.RIGHT: return -Math.PI / 2; 
        case WalkKey.LEFT: return Math.PI / 2; 
        case WalkKey.DOWN: return Math.PI;
      }
      break;
    case WalkKey.DOWN:
      switch (direction){
        case WalkKey.RIGHT: return forwordDirection * -Math.PI / 2; 
        case WalkKey.LEFT: return forwordDirection * Math.PI / 2; 
        case WalkKey.UP: return -Math.PI;
      }
      break;
  }
}
/********************************** END : USER ANIMATION *************************************/

/********************************** START : TEXT TO SHOW WHEN USER APPROACHES PERSONS *************************************/
function makeTextSprite(message, parameters) {
  // Canvas for text
  const textCanvas = prepareTextCanvas(message, parameters);

  // Canvas Texture
  const texture = new THREE.CanvasTexture(textCanvas);
  texture.minFilter = THREE.LinearFilter;
  texture.wrapS = THREE.ClampToEdgeWrapping;
  texture.wrapT = THREE.ClampToEdgeWrapping;

  // Sprite Material
  const textMaterial = new THREE.SpriteMaterial({
    map: texture,
    transparent: true,
  });

  // Sprite
  const textSprite = new THREE.Sprite(textMaterial);
  return textSprite;
}

function prepareTextCanvas(message, parameters) {
  if ( parameters === undefined ) parameters = {};
  var fontface = parameters.hasOwnProperty("fontface") ? parameters["fontface"] : "Calibri";
  var fontsize = parameters.hasOwnProperty("fontsize") ? parameters["fontsize"] : 24;
  var borderThickness = parameters.hasOwnProperty("borderThickness") ? parameters["borderThickness"] : 4;
  var borderColor = parameters.hasOwnProperty("borderColor") ?parameters["borderColor"] : "white";
  var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?parameters["backgroundColor"] : "rgba(0,0,0,0.6)";
  var textColor = parameters.hasOwnProperty("textColor") ?parameters["textColor"] : "white";
  var isCallOut = parameters.hasOwnProperty("isCallOut") ? parameters["isCallOut"] : true;

  var maxWidth = 280;
  var lineHeight = 30;
  
  var canvas = document.createElement('canvas');
  var context = canvas.getContext('2d');
  if(isCallOut) {
    createCallOut(context, backgroundColor, canvas.width, 108)
  } else {
    maxWidth = 50;
    createOvalBackground(context, backgroundColor)
  }

  var x = (canvas.width - maxWidth) / 2; // default canvas width-300, height-150
  var y = 35;
  context.font = fontsize + "px " + fontface;
  context.strokeStyle = borderColor;
  context.lineWidth = borderThickness;
  context.fillStyle = textColor;

  wrapText(context, message, x, y, maxWidth, lineHeight);
  return context.canvas;
}

function wrapText(context, text, x, y, maxWidth, lineHeight) {
  var words = text.split(' ');
  var line = '';
  for(var n = 0; n < words.length; n++) {
    var testLine = line + words[n] + ' ';
    var metrics = context.measureText(testLine);
    var testWidth = metrics.width;
    if (testWidth > maxWidth && n > 0) {
      context.fillText(line, x, y);
      line = words[n] + ' ';
      y += lineHeight;
    }
    else {
      line = testLine;
    }
  }
  context.fillText(line, x, y);
}

function createCallOut(context, backgroundColor, width, height) {
  context.lineTo(0,10);
  context.lineTo(width, 10);
  context.lineTo(width, height);
  context.lineTo(60, height);
  context.lineTo(77,130);
  context.lineTo(27, height);
  context.lineTo(0, height);
  context.lineTo(0,10);
  context.closePath();
  context.stroke()
  context.fillStyle = backgroundColor
  context.fill();
}

function createOvalBackground(context, backgroundColor) {
  context.beginPath();
  context.arc(135, 25, 20, 0, 2 * Math.PI);
  context.stroke();
  context.fillStyle = backgroundColor
  context.fill();
}

function createObject3DPlane() {
  var geometry = new THREE.PlaneGeometry(300, 300, 10, 10 );
  var material = new THREE.MeshBasicMaterial( {color: 0xffff00, side: THREE.DoubleSide} );
  var plane = new THREE.Mesh( geometry, material );
  plane.visible = false;
  plane.rotation.x = Math.PI/2;
  scene.add(plane)
  return plane;
}

function setUserDataOf3DModelsForText(object, text, textType) {
  var box = new THREE.Box3().setFromObject(object);
  var objectUserData = {
    textSprite: {
      message: text,
      type: textType,
      position: {
        x: (box.max.x + box.min.x)/2,
        y: box.max.y + 60,
        z: (box.max.z + box.min.z)/2
      }
    }
  }
  return objectUserData;
}

function getTextSpriteBasedOnSection(section) {
  switch (section) {
    case constants.section.game:
      return gameTextSprite;
    case constants.section.gallery:
      return galleryTextSprite;
    case constants.section.library:
      return libraryTextSprite;
    case constants.section.intro:
      return introTextSprite;
  }
}


var IsOnGallaryPodium = 0
var myTimer = null;
var counter = 0;
var interval = null
function setMyTimer(podiumType){
  myTimer = setTimeout(
    function(){ 
      window.location.href = constants.ggk_geek_baseURL + getPodiumURL(podiumType) 
    }, 5000);
}
function setMyTimeInterval(textData){
  interval = setInterval(function() {
    if (messageSprite) {
      scene.remove(messageSprite);
    }
    var c = ++counter
    messageSprite = makeTextSprite(c.toString(), {fontsize: 40, isCallOut: false})
    scene.add(messageSprite);
    var textPosition = textData.position;
    messageSprite.position.set(textPosition.x, textPosition.y + 200, textPosition.z);
    messageSprite.scale.set(200,150,1);
  }, 1000);
}

function showHideTextSprite() {
  var pos = man_walk.position.clone();
  camera.updateMatrixWorld();
  pos.project(camera);
  raycaster.setFromCamera({ x: pos.x, y: pos.y }, camera);
  var intersects = [];
  intersects = raycaster.intersectObjects(interactiveObjects, true);
  if (intersects.length > 0 && intersects[0].object.objectType == 'podium') {
    var textData = intersects[0].object.userData.textSprite;
    if(!IsOnGallaryPodium) {
      setMyTimer(textData.type)
      setMyTimeInterval(textData)
     }
     IsOnGallaryPodium = 1;
  } else if (intersects.length > 0 && intersects[0].object.objectType != 'podium') {
      var textData = intersects[0].object.userData.textSprite;
      messageSprite = getTextSpriteBasedOnSection(textData.type);
      scene.add(messageSprite);
      var textPosition = textData.position;
      messageSprite.position.set(textPosition.x, textPosition.y, textPosition.z);
      messageSprite.scale.set(200,150,1);
      resetTimer()
  } else {
    resetTimer()
    if (messageSprite) {
      scene.remove( messageSprite );
    }
  }
}

function resetTimer(){
  if(myTimer) {
    clearTimeout(myTimer)
    myTimer = null
    clearInterval(interval);
    interval = null
    counter = 0
  }
  IsOnGallaryPodium = 0
}
/************************************* END : TEXT TO SHOW WHEN USER APPROACHES PERSONS *************************************/

/************************************* START : HEXAGON STAGES *************************************/
function generateHexagonStage(extrudeSettings, x, z) {
  var group = new THREE.Group()
  var hex1 = generate3DHexagon(140, extrudeSettings, 0x00f000, x, 8, z)
  group.add(hex1)

  var hex2 = generate3DHexagon(120, extrudeSettings, 0xff00ff, x, 16, z)
  group.add(hex2)

  var hex3 = generate3DHexagon(100, extrudeSettings, 0x00f000, x, 24, z)
  group.add(hex3)
  return group
}

function generate3DHexagon(size, extrudeSettings, color, x, y, z) {
  var hexShape = getHexagonShape(size);
  var hexagon = getHexagonMesh(hexShape, extrudeSettings, color, x, y, z)
  return hexagon
  // scene.add(hexagon);
}

function getHexagonMesh(shape, extrudeSettings, color, x, y, z) {
  var geometry = new THREE.ExtrudeBufferGeometry( shape, extrudeSettings );
  var mesh = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial( { color: color } ) );
  mesh.position.set( x, y, z - 75 );
  mesh.rotation.x = Math.PI/2;
  return mesh;
}

function getHexagonShape(cellSize) {
  // create base shape used for building geometry
  var i, verts = [];
  // create the skeleton of the hex
  for (i = 0; i < 6; i++) {
    verts.push(createVertex(i, cellSize));
  }
  // copy the verts into a shape for the geometry to use
  var cellShape = new THREE.Shape();
  cellShape.moveTo(verts[0].x, verts[0].y);
  for (i = 1; i < 6; i++) {
    cellShape.lineTo(verts[i].x, verts[i].y);
  }
  cellShape.lineTo(verts[0].x, verts[0].y);
  cellShape.autoClose = true;
  return cellShape
}

function createVertex(i, cellSize) {
  var angle = (Math.PI * 4 / 6) * i;
  return new THREE.Vector3((cellSize * Math.cos(angle)), (cellSize * Math.sin(angle)), 0);
}
/************************************* END : HEXAGON STAGES *************************************/

/************************************* START : CIRCLE STAGES *************************************/
function generateCircleStage(extrudeSettings) {
  generate3DCircle(100, extrudeSettings, 0x00f000, 8)
  generate3DCircle(80, extrudeSettings, 0xffffff, 16)
  generate3DCircle(60, extrudeSettings, 0x00f000, 24)
}

function generate3DCircle(radius, extrudeSettings, color, y) {
  var circleShape = getCircleShape(radius);
  var circle = getCircleMesh( circleShape, extrudeSettings, color, 0, y, 0, 0, 0, 0, 1 );
  circle.rotation.x = Math.PI/2;
  scene.add(circle);
}

function getCircleMesh( shape, extrudeSettings, color, x, y, z, rx, ry, rz, s ) {

  // extruded shape
  var geometry = new THREE.ExtrudeBufferGeometry( shape, extrudeSettings );

  var mesh = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial( { color: color } ) );
  mesh.position.set( x, y, z - 75 );
  mesh.rotation.set( rx, ry, rz );
  // mesh.scale.set( s, s, s );
  return mesh
}

function getCircleShape(radius) {
  var circleShape = new THREE.Shape()
    .moveTo( 0, radius )
    .quadraticCurveTo( radius, radius, radius, 0 )
    .quadraticCurveTo( radius, - radius, 0, - radius )
    .quadraticCurveTo( - radius, - radius, - radius, 0 )
    .quadraticCurveTo( - radius, radius, 0, radius );
  return circleShape
}
/************************************* END : CIRCLE STAGES *************************************/
