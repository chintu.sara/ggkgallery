import { OrbitControls } from '../../../assets/js/OrbitControls.js';
import { GLTFLoader } from '../../../assets/js/GLTFLoader.js';

var camera, scene, renderer, controls;
var gltfLoader = new GLTFLoader();

init()
animate();

function init() {

    // RENDERER
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.outputEncoding = THREE.sRGBEncoding;
    renderer.shadowMap.enabled = true;
    document.body.appendChild(renderer.domElement);

    // SCENE
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xDDDDDD );

    // CAMERA
    camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.1, 100000 );
    camera.position.y = 250;
    camera.position.z = 900;

    // LIGHTS
    const light = new THREE.AmbientLight(0xffffff);
    light.position.set = (0,100,100);
    scene.add(light);

    var dlight = new THREE.DirectionalLight( 0xaabbff, 0.3 );
    dlight.position.x = 0;
    dlight.position.y = 750;
    dlight.position.z = 0;
    scene.add(dlight);

    // ORBIT CONTROLS
    controls = new OrbitControls(camera, renderer.domElement);
	controls.maxPolarAngle = Math.PI * 0.5;
	controls.minDistance = 1000;
	controls.maxDistance = 5000;
    loadGallery();

}

function animate() {
    requestAnimationFrame(animate);
    render();
}

function render() 
{
    controls.update();
	renderer.render( scene, camera );
}

function loadGallery() {
    gltfLoader.load('/assets/3d_models/newsletter_gallery/scene.gltf', gltf => {
        scene.add(gltf.scene);
		gltf.scene.scale.multiplyScalar(50);
		gltf.scene.rotation.y = Math.PI/2
    });
}