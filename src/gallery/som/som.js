import { OrbitControls } from '../../../assets/js/OrbitControls.js';
import { GLTFLoader } from '../../../assets/js/GLTFLoader.js';

var camera, scene, renderer, controls;
var gltfLoader = new GLTFLoader();

init()
animate();

function init() {

    // RENDERER
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.outputEncoding = THREE.sRGBEncoding;
    renderer.shadowMap.enabled = true;
    document.body.appendChild(renderer.domElement);

    // SCENE
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xC3D8D6 );

    // CAMERA
    camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 100000 );
    camera.position.y = 250;
    camera.position.z = 1500;

    // LIGHTS
    const light = new THREE.AmbientLight(0xffffff);
    light.position.set = (0,100,100);
    scene.add(light);

    // ORBIT CONTROLS
    controls = new OrbitControls(camera, renderer.domElement);
    controls.enableDampling = true;
    controls.campingFactor = 0.25;
    controls.enableZoom = true;

    loadGallery();
}

function animate() {
    requestAnimationFrame(animate);
    render();
}

function render() 
{
    controls.update();
	renderer.render( scene, camera );
}

function loadGallery() {
    gltfLoader.load('/assets/3d_models/landscape-gallery/scene.gltf', gltf => {
        gltf.scene.traverse( function ( object ) {
            if (object.isMesh) {
                if (object.name == "Cube003_4") {
                    updatePicture(object, '/assets/img/person1.png')
                } else if (object.name == "Cube003_2") {
                    updatePicture(object, '/assets/img/person2.png')
                 }else if (object.name == "Cube003_7") {
                    updatePicture(object, '/assets/img/person3.png')
                }
            }
        });
        scene.add(gltf.scene);
        gltf.scene.scale.multiplyScalar(1000);
    });
}

function updatePicture(object, imagePath) {
    var newTexture = new THREE.TextureLoader().load( imagePath );
    newTexture.encoding = THREE.sRGBEncoding;
    object.material.map = newTexture;
    object.material.needsUpdate = true; 
}