function fadeOutEffect() {
    var fadeTarget = document.getElementById("loading");
    var fadeEffect = setInterval(function () {
        if (!fadeTarget.style.opacity) {
            fadeTarget.style.opacity = 1;
        }
        if (fadeTarget.style.opacity > 0) {
            fadeTarget.style.opacity -= 0.1;
        } else {
            clearInterval(fadeEffect);
        }
    }, 2000);
    document.getElementById("loading").style.display = "none";
    document.getElementById("controls").style.display = "block";
  }
  document.getElementById("loading").style.display = "block";
  document.getElementById("controls").style.display = "none";
  window.onload = function() {
    setInterval(function(){
      fadeOutEffect();
    }, 1000);
  }
function enterWorld(){
  document.getElementById("controls").remove();
}